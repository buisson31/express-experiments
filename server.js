const express = require('express')
const path = require('path')

const app = express()

app.set("views", path.join(__dirname, "views"))
app.set("view engine", "pug")

app.use(express.static(path.join(__dirname, "public")))

app.get("/", (req, res) => {
   res.render("index", {
      name: "Chris",
      url: "https://wiki.formation-fullstack.fr",
   })
})

app.listen(3000, function () {
  console.log("Server listening on port 3000")
})
